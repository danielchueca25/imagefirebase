package com.daniel.imagecamera;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailsActivity extends AppCompatActivity {
    ImageView imageBig;
    Button delete;
    Button download;
    private final String DIRECTORY_DOWNLOADS = "storage/emulated/0/Download";
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        imageBig = findViewById(R.id.imageBig);
        delete = findViewById(R.id.delete);
        download = findViewById(R.id.download);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FirebaseStorage storage= FirebaseStorage.getInstance();
        String imageUrl = getIntent().getStringExtra("imageBig");
        Picasso.get().load(imageUrl)
                .fit()
                .centerCrop()
                .into(imageBig);
        StorageReference httpsReference = storage.getReferenceFromUrl(imageUrl);
        delete.setOnClickListener(v -> {
            deleteImage(httpsReference);
            Intent intent =new Intent(getApplicationContext(), ListActivity.class);
            startActivity(intent);
        });
        download.setOnClickListener(v -> downloadFile(getApplicationContext(),
                createFileName(), ".jpg", DIRECTORY_DOWNLOADS, imageUrl));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.camara:
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                return true;
            case R.id.list:
                Intent intent = new Intent(getApplicationContext(), ListActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void deleteImage(StorageReference httpsReference) {
        httpsReference.delete()
                .addOnSuccessListener(unused -> Toast.makeText(getApplicationContext(), "Image deleted",
                Toast.LENGTH_SHORT).show())
                .addOnFailureListener(e -> Toast.makeText(getApplicationContext(), "Error deleting image",
                        Toast.LENGTH_SHORT).show());
    }

    public void downloadFile(Context context, String fileName, String fileExt, String destination,
                             String url) {
        DownloadManager downloadManager = (DownloadManager)
                context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalFilesDir(context, destination, fileName + fileExt);
        downloadManager.enqueue(request);
    }
    private String createFileName() {
        // Create an image file name with date format and starting with JPEG
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp;
        return imageFileName;
    }


}